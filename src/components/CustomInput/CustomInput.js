import React from 'react';
import './CustomInput.css';

export default function CustomInput({
    label,
    onChange,
    placeholder,
    secondary,
    errorStateActive,
    errorText,
    disabled,
}) {
    const styles = `${secondary ? 'input-secondary' : ''} ${errorStateActive ? 'input-active-error' : ''}`;
    return (
        <>
        <label>
        { label }
        <input
            placeholder={placeholder}
            onChange={onChange}
            className={styles}
            disabled={disabled}
        />
        </label>
        {
            errorStateActive
            ? (
                <p className="error">{errorText}</p>
            )
            : null
        }
        </>
    );
}