import React from 'react';
import './Button.css';

export default function Button({ text, secondary, disabled, onClick }) {
  return (
    <button
      className={secondary ? 'button-secondary' : 'button'}
      disabled={disabled}
      onClick={onClick}
    >
      { text }
    </button>
  );
}