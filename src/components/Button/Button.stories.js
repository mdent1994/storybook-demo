import React from 'react';
import Button from './Button';
import { action } from '@storybook/addon-actions';

/* Define the default component for our stories */
export default {
  component: Button,
  title: 'Button',
  // decorators: [withKnobs]
};

/* Export our different stories for each visual state */
export const Primary = () => <Button text="Primary Button"  onClick={action('clicked')} />;
export const Secondary = () => <Button text="Secondary Button" secondary />;
export const Disabled = () => <Button text="Disabled Button" disabled />;
