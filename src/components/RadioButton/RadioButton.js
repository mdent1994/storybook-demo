import React from 'react';
import './RadioButton.css';

export default function RadioButton({ text, disabled }) {
  return (
    <label>
      <input type="radio" disabled={disabled}/>
    </label>
  );
}